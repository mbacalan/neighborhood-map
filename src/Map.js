import React, { Component } from "react"
import escapeRegExp from "escape-string-regexp"

class Map extends Component {
  matchMarkers = () => {
    // Return if the map isn't initialized yet
    if (!this.props.map) {
      return;
    }
    if (this.props.query !== "") {
      // Check the locations title against query to find a match
      const matches = this.props.locations.filter(
        (location) => location.title.toLowerCase().search(escapeRegExp(this.props.query.toLowerCase())) !== -1)
      if (matches.length === 1 && matches[0].marker) {
        this.setMarkers(matches)
        // Toggle active on the filtered marker only if it isn't already active
        if (matches[0].marker.getAnimation() === null) {
          this.props.toggleActive(matches[0].marker)
        }
      }
      else if (matches.length > 1) {
        // Toggle active on the filtered markers only if they are already active
        matches.forEach(match => {
          if (match.marker.getAnimation() !== null) {
            this.props.toggleActive(match.marker)
          }
        })
        this.setMarkers(matches)
      } else {
        this.setMarkers(this.props.locations)
      }
    }
    else {
      this.setMarkers(this.props.locations)
    }
  }

  // Check the locations marker object and decide if it should be shown
  setMarkers = (shownMarkers) => {
    this.props.locations.forEach((location) => {
      if (shownMarkers.indexOf(location) !== -1) {
        if (location.marker.map !== this.props.map) {
          location.marker.setMap(this.props.map)
        }
      } else {
        location.marker.setMap(null)
      }
    })
  }

  render() {
    this.matchMarkers()
    return (
      <div id="map" role="application"></div>
    )
  }
}

export default Map
