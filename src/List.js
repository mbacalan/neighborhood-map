import React, { Component } from "react";
import escapeRegExp from "escape-string-regexp"

class List extends Component {
  state = {
    query: ""
  }

  render() {
    let shownLocation;
    if (this.state.query) {
      const match = new RegExp(escapeRegExp(this.state.query), "i")
      shownLocation = this.props.locations.filter((locations) => match.test(locations.title))
    } else {
      shownLocation = this.props.locations
    }

    return (
      <div id="list">
        <input
          type="text"
          value={this.props.query}
          placeholder="Enter area name"
          tabIndex="2"
          onChange={(event) => {
            this.setState({ query: event.target.value }, () => {
              this.props.onSetQuery(this.state.query)
            })
          }}
        />

        <input
          type="button"
          value="Filter"
          tabIndex="0"
          onClick={(e) => {
            e.preventDefault();
            this.props.onSetQuery(this.state.query)
          }}
        />

        <ul>
          {shownLocation.map((location) => (
            <li
              key={location.title}
              className="locations"
              tabIndex="0"
              onClick={(e) => { this.props.toggleActive(location.marker) }}
            >
              {location.title}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default List
