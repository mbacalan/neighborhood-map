export default function getFoursquareData(id, infowindow) {
  const clientID = "0EBDRQAHWF3XDZLA5C3M2IRBTQSFV5LHHN1PN453FPKNCZMA"
  const clientSecret = "D2N5OQNVP1IVLF5OSDRO5UK41QC5I0UCXCFOQDBYR0A440GS"
  const url = `https://api.foursquare.com/v2/venues/${id}?client_id=${clientID}&client_secret=${clientSecret}&v=20180803`

  fetch(url)
    .then(function (response) {
      return response.json();
    })
    .then((info) => {
      infowindow.setContent(
        `<div class="info-name">${info.response.venue.name}</div>
         <div class="info"><span class="info-phone">Contact Phone:</span> ${info.response.venue.contact.phone}</div>
         <div class="info"><span class="info-rating">Ratings:</span> ${info.response.venue.rating}</div>
         <span class="info-footer">Provided by FourSquare</span>`)
    })
    .catch(() => {
      infowindow.setContent("<p>Oops, there was an issue retrieving info from Foursquare!</p>")
    })
}
