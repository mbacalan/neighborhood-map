import React, { Component } from 'react';
import Map from "./Map"
import List from "./List"
import './App.css';
import getFoursquareData from "./foursquareAPI";


class App extends Component {
  state = {
    query: ""
  }

  locations = [
    { title: "Home", pos: { lat: 40.999554, lng: 28.789567 }, id: "4adcda10f964a520af3521e3" },
    { title: "Elementary School", pos: { lat: 40.996589, lng: 28.792990 }, id: "4b64444ff964a52054a72ae3" },
    { title: "High School", pos: { lat: 40.997634, lng: 28.786349 }, id: "4bccc4e7937ca59381baab92" },
    { title: "Swimming Pool", pos: { lat: 40.997707, lng: 28.792647 }, id: "4b97e079f964a520fb1b35e3" },
    { title: "Park", pos: { lat: 40.995175, lng: 28.793183 }, id: "4ba8b203f964a520a6e739e3" },
    { title: "Mosque", pos: { lat: 40.995107, lng: 28.792335 }, id: "4adcda09f964a520e83321e3" }
  ]

  componentDidMount() {
    window.initMap = this.initMap.bind(this);
    window.gm_authFailure = this.gm_authFailure;
    loadJS("https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAM4t1bhQbHyohh-p6JzH8PFH-61TMHpBk&callback=initMap")
  }

  setQuery = (query) => {
    this.setState({ query })
  }

  gm_authFailure = () => {
    document.querySelector(".gm-err-message")
      .innerHTML = "There seems to be an issue with your API key, please double check. See the JavaScript console for technical details."
  };

  initMap() {
    const google = window.google
    const styles = [
      {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
          { "visibility": "off" }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [
          { "visibility": "off" }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
          { "visibility": "off" }
        ]
      },
      {
        "featureType": "transit",
        "stylers": [
          { "visibility": "off" }
        ]
      }
    ]

    this.map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: 40.997142, lng: 28.789961 },
      zoom: 16,
      mapTypeControl: false,
      styles
    });

    // Create a marker for each location
    this.locations.forEach(location => {
      let marker = new google.maps.Marker({
        map: this.map,
        title: location.title,
        position: location.pos,
        animation: google.maps.Animation.DROP,
      });

      // Add info window and event handlers to markers
      marker.infowindow = new google.maps.InfoWindow({});
      marker.addListener("click", () => this.toggleActive(marker));
      marker.infowindow.addListener("closeclick", () => this.toggleActive(marker));
      getFoursquareData(location.id, marker.infowindow)
      // Add the marker object to the corresponding entry in location array
      location.marker = marker
    });
  }

  // Change zoom, marker animation and info window display to indicate an active marker
  toggleActive = (marker) => {
    if (marker.getAnimation() !== null) {
      marker.infowindow.close();
      marker.setAnimation(null);
      this.map.setCenter({ lat: 40.997142, lng: 28.789961 });
      this.map.setZoom(16);
    } else {
      marker.infowindow.open(this.map, marker);
      marker.setAnimation(window.google.maps.Animation.BOUNCE);
      this.map.setCenter(marker.position)
      this.map.setZoom(17);
    }
  }

  render() {
    return (
      <div className="app">
        <header>
          <h1>Neighborhood Map</h1>
        </header>
        <div className="flex">
          <List
            locations={this.locations}
            query={this.state.query}
            onSetQuery={this.setQuery}
            toggleActive={this.toggleActive}
          />
          <Map
            map={this.map}
            locations={this.locations}
            query={this.state.query}
            toggleActive={this.toggleActive}
          />
        </div>
      </div>
    );
  }
}

export default App;

function loadJS(src) {
  const script = document.createElement("script");
  script.type = "text/javascript";
  script.src = src;
  document.body.appendChild(script);
}
