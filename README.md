# Neighborhood Map

A React app that utilizes a map to show off a neighborhood.

The map is created via Google Maps JS API.
Markers are created from a list of hardcoded locations.
Clicking on a marker will zoom on its location, give it an animation and toggle it's info window.
Each info window is populated by details from Foursquare's Venues API endpoint.

There is also a list of locations and a text input to filter locations on demand.

## Installation

```bash
#Install package dependencies
npm install

#Start a development server
npm start

#Build for production
npm build

#Commands are interchangible with yarn
```

## API Dependencies

* Google Maps JavaScript API
* Foursquare API

## Notes

This project is part of [Udacity](https://www.udacity.com/)'s Front-End Web Developer Nanodegree.
